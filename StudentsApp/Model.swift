//
//  Model.swift
//  StudentsApp
//
//  Created by איתמר היגאני on 14/05/2022.
//

import Foundation
import UIKit
import CoreData

class Model{
    
    private init() { }
    
    static let instance = Model()
    var students: [Student] = [
        Student(id: "314751264", name: "Itamar Higani", phone: "0504590123", address: "Holon"),
        Student(id: "206711554", name: "Dor Cohen", phone: "0503595412", address: "Tel Aviv")
    ]

    func getAllStudents()->[Student]{
        return Model.instance.students
    }
    
    func add(student:Student) {
        let st = Student()
        st.id = student.id
        st.name = student.name
        st.phone = student.phone
        st.avatarUrl = student.avatarUrl
        st.address = student.address

        Model.instance.students.append(st)
    }
}

