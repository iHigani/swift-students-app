//
//  StudentListViewController.swift
//  StudentsApp
//
//  Created by איתמר היגאני on 14/05/2022.
//

import UIKit

class StudentListViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Model.instance.getAllStudents().count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StudentCell", for: indexPath) as! StudentTableViewCell
        let st = Model.instance.getAllStudents()[indexPath.row]
        cell.ID.text = st.id!
        cell.NAME.text = st.name!

        return cell
    }
    
    var selectedRow = 0
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NSLog("Selcted row at \(indexPath.row)")
        selectedRow = indexPath.row
        performSegue(withIdentifier: "openStudentDetailsSegue", sender: self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "openStudentDetailsSegue"){
            let dvc = segue.destination as! StudentDetailsViewController
            let st = Model.instance.getAllStudents()[selectedRow]
            dvc.student = st
        }
    }
}
