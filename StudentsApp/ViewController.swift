//
//  ViewController.swift
//  StudentsApp
//
//  Created by איתמר היגאני on 14/05/2022.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var viewForTab: UIView!
    @IBOutlet weak var contentView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showSceneByTag(tag: 0)
    }

    @IBAction func onTabBarClick(_ sender: UIButton) {
        showSceneByTag(tag: sender.tag)
    }
    
    func showSceneByTag(tag: Int) {
        if tag == 2 {
            guard let About =
                    self.storyboard?.instantiateViewController(withIdentifier: "AboutViewController") as? AboutViewController else { return }
            About.view.frame = contentView.frame
            About.view.frame.origin = CGPoint(x: 0, y: 0)
            self.addChild(About)
            contentView.addSubview(About.view)
            About.didMove(toParent: self)
        } else if tag == 1 {
            guard let NewStudent =
                    self.storyboard?.instantiateViewController(withIdentifier: "NewStudentFormViewController") as? NewStudentFormViewController else { return }
            NewStudent.view.frame = contentView.frame
            NewStudent.view.frame.origin = CGPoint(x: 0, y: 0)
            self.addChild(NewStudent)
            contentView.addSubview(NewStudent.view)
            NewStudent.didMove(toParent: self)
        } else if tag == 0 {
            guard let StudentList =
                    self.storyboard?.instantiateViewController(withIdentifier: "StudentListViewController") as? StudentListViewController else { return }
            
            let navigationController = UINavigationController(rootViewController: StudentList)
            navigationController.view.frame = contentView.frame
            navigationController.view.frame.origin = CGPoint(x: 0, y: 0)
            self.addChild(navigationController)
            contentView.addSubview(navigationController.view)
            navigationController.didMove(toParent: self)
        }
    }
}
