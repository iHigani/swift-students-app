//
//  NewStudentViewController.swift
//  StudentsApp
//
//  Created by איתמר היגאני on 14/05/2022.
//

import UIKit

class NewStudentFormViewController: UIViewController {


    @IBOutlet weak var id: UITextField!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var address: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("ITAMARRRRR")
        // Do any additional setup after loading the view.
    }

    func clearForm() {
        id.text = ""
        name.text = ""
        phone.text = ""
        address.text = ""
    }
    

    @IBAction func save(_ sender: Any) {
        let student = Student()
        student.id = id.text
        student.name = name.text
        student.phone = phone.text
        student.address = address.text

        Model.instance.add(student: student)

        clearForm()
    }
    
    
    @IBAction func cancel(_ sender: Any) {
        clearForm()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
