//
//  Student.swift
//  StudentsApp
//
//  Created by איתמר היגאני on 14/05/2022.
//

import Foundation

class Student {
    public var id: String?
    public var name: String?
    public var phone: String?
    public var address: String?
    public var avatarUrl: String?
    
    init () {}
    
    init (id: String, name: String, phone: String, address: String) {
        self.id = id
        self.name = name
        self.phone = phone
        self.address = address
    }
}
